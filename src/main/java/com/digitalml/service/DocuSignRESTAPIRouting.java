package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Context.Builder;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class DocuSignRESTAPIRouting {

    private static final Logger logger = LoggerFactory.getLogger("docusignrestapi:v2");

    private static final String cacheURL = "http://infocache-test:9200/docusignrestapi-v2";
    
    private static final String resourceName = "docusignrestapi";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "docusignrestapi"), value("apiversion", "v2"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/v2/accounts/:accountId/signing_groups/:signingGroupId/users", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/signing_groups/:signingGroupId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/signing_groups/:signingGroupId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/brands/:brandId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/brands/:brandId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/brands/:brandId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/folders/:folderId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/folders/:folderId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/views/recipient", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/cloud_storage/:serviceId/folders/:folderId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/users/:userId/cloud_storage/:serviceId/folders", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/templates/:templateId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/settings/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/settings/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/chunked_uploads/:chunkedUploadId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/chunked_uploads/:chunkedUploadId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/chunked_uploads/:chunkedUploadId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/connect/failures", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/tab_definitions", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/tab_definitions", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/signature", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/supported_languages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/cloud_storage/:serviceId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/users/:userId/cloud_storage/:serviceId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/settings/password_rules", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/settings/password_rules", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/connect/logs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/connect/logs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/lock", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/billing_invoices/:invoiceId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/brands/:brandId/resources/:resourceContentType", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/brands/:brandId/resources/:resourceContentType", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/signature_image", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/signature_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/custom_fields", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/custom_settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/custom_settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/custom_settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/billing_charges", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/pages/:pageNumber/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/users/:userId/signatures/:signatureId/:imageType", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/signatures/:signatureId/:imageType", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/signatures/:signatureId/:imageType", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/signatureProviders", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/fields", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/billing_plan", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/billing_plan", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/custom_fields", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/oauth2/revoke", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/pages/:pageNumber/page_image", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/pages/:pageNumber/page_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/chunked_uploads", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/connect/envelopes/retry_queue", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId/files/:fileId/pages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/consumer_disclosure/:langCode", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/consumer_disclosure/:langCode", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/current_user/password_rules", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/powerforms", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/powerforms", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/powerforms", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/unsupported_file_types", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/envelopes", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/bulk_envelopes/:batchId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/settings/enote_configuration", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/settings/enote_configuration", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/settings/enote_configuration", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/login_information", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/recipients/document_visibility", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/diagnostics/request_logs/:requestLogId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/views/console", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/profile/image", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/profile/image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/profile/image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/document_visibility", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/initials_image", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/initials_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/connect/failures/:failureId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/connect/:connectId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/connect/:connectId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/custom_fields/:customFieldId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/custom_fields/:customFieldId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/folders", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/watermark/preview", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/notification", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/notification", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/document_visibility", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/document_visibility", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/attachments", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/attachments", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/attachments", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/recipient_names", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/templates/:templateId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/billing_plan/credit_card", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/consumer_disclosure", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/service_information", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/provisioning", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/brands", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/brands", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/brands", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/views/edit", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/billing_payments", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/billing_payments", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/:templatePart", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/:templatePart", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/consumer_disclosure", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/lock", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/lock", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/recipients", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/billing_invoices_past_due", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/documents", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/documents", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/diagnostics/settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/diagnostics/settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/pages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/payment_gateway_accounts", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/connect/envelopes/:envelopeId/retry_queue", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/notification", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/notification", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/eMortgage/transactions", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/signatures/:signatureId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/signatures/:signatureId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/signatures/:signatureId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/powerforms/senders", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/billing_plan/purchased_envelopes", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/attachments/:attachmentId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/attachments/:attachmentId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/search_folders/:searchFolderId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/users/:userId/profile", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/profile", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/captive_recipients/:recipientPart", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/contacts", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/contacts", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/contacts", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/watermark", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/watermark", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/views/sender", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/billing_invoices", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/pages/:pageNumber/page_image", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/pages/:pageNumber/page_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/connect/:connectId/users", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/diagnostics/request_logs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/diagnostics/request_logs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/form_data", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/templates", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/templates", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/groups/:groupId/brands", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/groups/:groupId/brands", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/groups/:groupId/brands", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/views/edit", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/brands/:brandId/logos/:logoType", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/brands/:brandId/logos/:logoType", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/brands/:brandId/logos/:logoType", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/custom_fields", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/custom_fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/signing_groups", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/signing_groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/signing_groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/signing_groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/powerforms/:powerFormId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/powerforms/:powerFormId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/powerforms/:powerFormId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/pages/:pageNumber", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/document_visibility", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/document_visibility", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/fields", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/fields", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/groups/:groupId/users", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/groups/:groupId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/groups/:groupId/users", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId/pages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/bulk_envelopes", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/billing_plans/:billingPlanId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/connect/logs/:logId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/connect/logs/:logId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/documents", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/documents", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId/files/:fileId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId/files/:fileId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/oauth2/token", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/pages/:pageNumber", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/workspaces/:workspaceId/folders/:folderId/files", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/audit_events", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/users/:userId/cloud_storage", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/users/:userId/cloud_storage", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/cloud_storage", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/workspaces", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/workspaces", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/social", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/social", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/users/:userId/social", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/email_settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/email_settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/email_settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/email_settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/views/correct", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/powerforms/:powerFormId/form_data", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/users/:userId/signatures", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/signatures", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/users/:userId/signatures", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/permission_profiles/:permissionProfileId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/permission_profiles/:permissionProfileId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/permission_profiles/:permissionProfileId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/pages/:pageNumber/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/chunked_uploads/:chunkedUploadId/:chunkedUploadPartSeq", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/bulk_recipients", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/connect", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/connect", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/connect", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/contacts/:contactId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        delete("/v2/accounts/:accountId/contacts/:contactId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/login_information/:loginPart", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/brands/:brandId/resources", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/templates", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/templates", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/users/:userId/settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/users/:userId/settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/billing_plans", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/groups", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/groups", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/permission_profiles", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/permission_profiles", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/documents/:documentId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/documents/:documentId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        put("/v2/accounts/:accountId/envelopes/status", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/recipients/:recipientId/consumer_disclosure/:langCode", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/billing_payments/:paymentId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/brands/:brandId/file", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/templates", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/v2/accounts/:accountId/envelopes/:envelopeId/documents/:documentId/templates", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/shared_access", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/shared_access", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/signing_groups/:signingGroupId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/signing_groups/:signingGroupId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/tab_definitions/:customTabId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/tab_definitions/:customTabId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/tab_definitions/:customTabId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/tabs", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/templates/:templateId/recipients/:recipientId/tabs", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/v2/accounts/:accountId/workspaces/:workspaceId", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        put("/v2/accounts/:accountId/workspaces/:workspaceId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.put(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        delete("/v2/accounts/:accountId/workspaces/:workspaceId", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.delete(cacheURL + "/" + resourceName + "/" + req.params("id")).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
    }
}